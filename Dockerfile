# Multistage build with docker layer caching
FROM node:14-alpine as builder
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY . .
RUN --mount=type=secret,id=dotenv,target=.env \
    npm run build

FROM nginx:stable
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
COPY ./default.conf /etc/nginx/conf.d/default.conf
