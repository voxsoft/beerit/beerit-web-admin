export interface User {
  id: number;
  name: string;
  image: {url: string, id: number};
  email: string;
}