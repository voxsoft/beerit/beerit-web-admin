import { createContext } from "react";
// import ReactDOM from "react-dom/client";

const ModalContext = createContext(null);

export default ModalContext;