import React, { useState, useContext, useEffect } from 'react';
import ModalListContext from '../context/modalListContext';
import { Brewery } from '../models/Brewery.model';
import BreweryForm from '../components/forms/BreweryForm';
import BeerButton, { ButtonStyle } from '../components/BeerButton';
import { ApiRequestProps, useAPI } from '../hooks/useAPI';
import BreweryCard from '../components/BreweryCard';
import Pagination from '../components/listUtils/Pagination';
import ListFilters, { FilterType } from '../components/listUtils/ListFilters';
import { countryCodes } from '../data/countries';

const countries = [{value: null, label: 'All countries'}, ...countryCodes.map(v => ({value: v.code, label: v.name}))];
const initFilters = {
    name: {defaultValue: '', placeholder: 'Name', type: FilterType.STRING},
    country: {defaultValue: countries[0].value, type: FilterType.SELECT, options: countries}
}

const Breweries = () => {
    const perPage = 12;
    const {addModal, closeModal} = useContext<any>(ModalListContext);
    const [request, setRequest] = useState<ApiRequestProps>({
        method: 'get', 
        path: `${process.env.REACT_APP_API_URL}/entity/Brewery/list`,
        params: {include: ['FileReference'], sort: [['id', 'DESC']], pagination: {page: 0, perPage: perPage}}
    });
    const {data} = useAPI<{items: Brewery[], totalItems: number}>(request);
    const breweries = data?.items ? data.items : [];
    const totalPages = data ? Math.ceil(data.totalItems / perPage) : 0;

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'});
    }, [data])

    const refresh = () => {
        setRequest({...request});
    }

    const openEditor = (brewery?: Brewery) => {
        if (brewery) {

        } else {
            addModal({
                content: <BreweryForm 
                    created={() => {
                        closeModal();
                        refresh();
                    }}
                ></BreweryForm>,
                size: {
                    maxWidth: 300,
                    maxHeight: 'fit-content'
                }
            });
        }
    }

    const pageChanged = (pageNum: number) => {
        if (!request) return;
        if (pageNum !== request.params.pagination.page) {
            setRequest({...request, params: {...request.params, pagination: {...request.params.pagination, page: pageNum}}});
        }
    }

    const filtersChanged = (filters: any) => {
        const filtersQuery: any = { ...filters };
        if (filtersQuery.country === null) {
           delete filtersQuery.country;
        }
        if (filtersQuery.name.trim() === '') {
            delete filtersQuery.name;
        } else {
            filtersQuery.name = {$like: `%${filtersQuery.name.trim()}%`}
        }
        setRequest({
          ...request,
          params: {
            ...request.params,
            pagination: {page: 0, perPage: perPage},
            where: filtersQuery
          }
        })
    }

    return (
        <div className='container pt-[50px] mx-auto'>
            <div className='text-3xl mb-8 font-bold text-gray-800 relative border-b-2 border-b-dark-blue mx-6'>
                BREWERIES
            </div>
            <div className='flex w-full'>
                <div className='basis-full md:basis-1/4'>
                    <ListFilters filters={initFilters} changed={filtersChanged} />
                    <div className='pl-6 mt-6'><BeerButton style={ButtonStyle.OUTLINE} text='Add brewery' onClick={openEditor} /></div>
                </div>
                <div className='basis-full md:basis-3/4'>
                    <div className=' grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-y-10 px-6 pb-10'>
                        {breweries ? breweries.map((brewery: Brewery, index: number) => <div className='flex justify-center' key={'brewery-' + index}>
                            <BreweryCard breweryData={brewery}></BreweryCard>
                        </div>) : null}
                    </div>
                    <Pagination className='pb-10' currentPage={request ? request.params.pagination.page + 1 : 1}
                    totalPages={totalPages} changed={(num: number) => pageChanged(num - 1)} />
                </div>
            </div>
        </div>
    );
}

export default Breweries;
