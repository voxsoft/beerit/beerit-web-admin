import React from 'react';

const PageHeader = ({title, color}: {title: string, color?: string}) => {
    return (
        <div className={`w-full text-3xl mb-8 text-center font-bold ${color ? 'text-' + color : 'text-dark-blue'} `}>{title}</div>
    );
}

export default PageHeader;
