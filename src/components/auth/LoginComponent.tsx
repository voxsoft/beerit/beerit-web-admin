import React, { useState } from 'react';
import Axios from 'axios';
import { useAuth } from '../../hooks/useAuth';
import BeerButton from '../BeerButton';

const LoginComponent = ({finished}: {finished: Function}) => {
  // const { token } = useContext(AuthContext);
  const { login } = useAuth();
  const [loginForm, setLoginForm] = useState({
    login: '',
    password: ''
  });

  const submit = () => {
    Axios.post(`${process.env.REACT_APP_API_URL}/auth/login`, loginForm).then(
      res => {
        const authToken = res.data.token;
        login(authToken);
        finished();
      },
      rej => { }
    )
  }

  return <div className="px-4 mx-auto mt-6">
      <div className='w-full'>
        <label>Login</label>
        <input className='block w-full border rounded-sm h-[36px] mt-2 mb-6 px-3' value={loginForm.login} onChange={event => {
          const value = event.target.value;
          setLoginForm(current => ({ ...current, login: value }))
        }} />
      </div>
      <div className='w-full'>
        <label>Password</label>
        <input type='password' className='block w-full border rounded-sm h-[36px] mt-2 mb-6 px-3' value={loginForm.password} onChange={event => {
          const value = event.target.value;
          setLoginForm(current => ({ ...current, password: value }))
        }} />
      </div>
      <div className='text-center'>
        <BeerButton text='Submit' onClick={submit} />
      </div>
    </div>
}

export default LoginComponent;
